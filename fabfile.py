from fabric import Connection
from invoke import run

def deploy():
    run('git add -A && git commit -m "autocommit" && git push origin master')
    c = Connection('root@focalise.ie')
    result = c.run('cd /var/www/html/wp-content/themes/focalise-3 && sh pulldown.sh')
    print(result)
    run('git push origin master')


deploy()