@extends('layouts.app') @section('content')

<div class="jumbotron gradient-bg jumbotron-fluid">
  <div class="container">
    <h1>Training</h1>
  </div>
</div>

<div class="container">
  <div class="card mb-5 training-card">
    <div class="card-body">
      <h4 class="card-title training-card-title">WordPress Training</h4>
      <p class="card-text">Learn how to build and manage websites with WordPress. We offer group courses and customised training for individuals and groups of staff. In-person classes are available in Dublin and Wicklow, and online classes are available if you are located elsewhere.</p>
      <a href="{{ home_url('/wordpress-training/') }}" class="btn btn-primary">Learn More</a>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">
        <i class="fa fa-check"></i>1-Day Courses</li>
        <li class="list-group-item">
        <i class="fa fa-check"></i>Group Classes</li>
      <li class="list-group-item">
        <i class="fa fa-check"></i>One to One Training</li>
      <li class="list-group-item">
        <i class="fa fa-check"></i>Beginner to Advanced</li>
    </ul>
  </div>

  <div class="card mb-5 training-card">
    <div class="card-body">
      <h4 class="card-title training-card-title">Computer Training For Adults</h4>
      <p class="card-text">Improve your computer skills and get the most out of your devices.</p>
      <a href="{{ home_url('/computer-training-for-adults/') }}" class="btn btn-primary">Learn More</a>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item"><i class="fa fa-check"></i>
      Training for iPads, iPhones and Android smartphones and tablets
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
      Apple MacBooks and MacOS training.
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
      Training Windows laptops and PCs.
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
        Using Email and Gmail 
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
        Microsoft Office (Microsoft Excel, Microsoft Word, Microsoft Powerpoint, G Suite and other popular business tools.
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
      Social Media Training
      </li>
      <li class="list-group-item"><i class="fa fa-check"></i>
        Adobe Photoshop and Lightroom training for Photographers.
      </li>
    </ul>
  </div>

</div>

@endsection
