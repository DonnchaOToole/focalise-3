@extends('layouts.app') @section('content')


<div class="jumbotron jumbotron-fluid gradient-bg">
  <div class="container">
      <i class="fa fa-laptop fa-5x mb-4"></i>
      <h1>How to add a custom domain to github pages with namecheap</h1>
  </div>
</div>


<div class="container">
<section>

<p class="lead">Wondering how you can set up your new domain from <a href="https://www.namecheap.com">Namecheap.com</a> with the wonderful <a href="https://pages.github.com">GitHub Pages</a>?</p>
I thought I'd share the settings that worked for me and hopefully save you some time.
</section>
<section>It's a pretty straightforward process, but it can be a little confusing at first, particularly if you are not yet adept with
Namecheap or GitHub Pages.</section>

<section>
<h3>Why should I use GitHub pages?</h3>
Websites that are hosted on GitHub pages load nice and quickly thanks to GitHub's infrastructure, and the hosting is free,
and you can't argue with that.

You can host personal, organisation, or project pages on GitHub.

You can host sites that handle plenty of traffic too, for example, fontawesome.io runs on GitHub Pages.

</section>

<section>
<h3>How do I link my domain to GitHub pages from the Namecheap dashboard?</h3>
Step 1: Buy your lovely new domain from Namecheap. Once you’ve bought your domain, log in to your
<a href="https://www.namecheap.com">Namecheap</a> dashboard, where you’ll find a list of all your domains. Click on the
<strong>manage</strong> button on the domain you’re assigning to GitHub Pages, and then click the <code><strong>Advanced DNS</strong></code> tab.

Next, click on <strong>Manage Your Host Records</strong> to add some tasty DNS records.

</section>

<section>
<h3>Add two A records pointing at GitHub pages</h3>
You want to map your domain name (<em>www.yoursite.com</em>) onto an IP address so that when visitors type in your domain name, their browser gets directed to the servers that host your website. We’re going to add two A records, with <strong>Host</strong> both set to <code>@</code>

The first one should point at
<code>192.30.252.153</code>

The second A record should point to
<code>192.30.252.154</code>

These IP addresses are the GitHub Pages servers.

</section>

<section>
<h3>Add a CNAME record</h3>
Next, we’ll add a <strong>CNAME</strong> record, with a <strong>www </strong>host set to:
<blockquote class="blockquote"><code>yoursite.github.io</code></blockquote>
After that, you’ll want to <strong>switch off</strong> the preexisting URL Record with the type <strong>URL redirect</strong>. If you don’t switch this off, you may find that Chrome throws a <strong>Too many redirects</strong> error, while Safari doesn’t seem to care.

</section>

<section>
<h3>Create a text file called CNAME in your repository</h3>
In your repo, create a text file called <strong>CNAME </strong>(with no file extension) and insert your new domain name in there, without the www, so it looks like this:
<blockquote class="blockquote"><code>yoursite.com</code></blockquote>
</section>

You can do this from within your repo locally, and push it to GitHub, or you can use the GitHub interface to create it.

<h4>Alternatively, create cname with the github interface</h4>

<p>Go to your GitHub Page repo and click on <em>Custom Domain</em>.</p>

<p>In the input box, type the domain name you just bought to set your GitHub Page too, e.g.
<code>
www.yourdomainname.com</code>

<section>
<h3>You're done, now wait for DNS propagation</h3>
That's it.

<code>git push</code> your repo to GitHub, give it a little while for the DNS to propagate through the internet, and then enjoy
your new cheap and cheerful GitHub Pages site. While you're waiting for your DNS updates to propagate, you can check how
the process is going using <a href="https://www.whatsmydns.net/">whatsmydns.net</a>
</p>

<p>
If you're still having trouble, here's the <a href="https://help.github.com/articles/using-a-custom-domain-with-github-pages/">documentation from GitHub.</a> If you'd like to have a chat with me, you can send me a mail at
<code>donncha@focalise.ie</code>.</p>
<p>I'm always interested to hear any feedback or suggestions for more tutorials. I'm also planning on producing lots of tutorials on YouTube, you can find the channel <a href="https://www.youtube.com/channel/UCAo1-eYYQL7UzYgigO8hHVQ">here</a>. Topic requests are welcome. We can also help you with <a href="https://focalise.ie/web-design/"><strong>web design, </strong></a><a href="https://focalise.ie/seo-services/"><strong>SEO</strong></a> and <a href="https://focalise.ie/web-hosting/">web hosting.</a>
</p>

<p>
Thanks for reading, and good luck with your new website!
</p>
</section>
</div>

@endsection
