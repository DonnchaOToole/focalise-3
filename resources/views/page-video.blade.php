@extends('layouts.app') @section('content')

<!--<img rel="preload" src="<?php bloginfo('template_directory');?>/dist/images/video-production2.jpg" alt="" class="img-fluid">-->

<div class="jumbotron jumbotron-fluid mb-0 gradient-bg">
  <div class="container">
    <div class="col-md-8 offset-md-2">
      <h1>Video Production</h1>
      <p class="lead">Motion Design • Editing • Events</p>
    </div>
  </div>
</div>


<div class="embed-responsive embed-responsive-16by9 mb-2">
  <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/102935516?title=0&byline=0&portrait=0" webkitallowfullscreen
    mozallowfullscreen allowfullscreen></iframe>
</div>
<div class="container">
  <p>We produced this trailer for <a href="https://thehappypear.ie">The Happy Pear</a>'s first cookbook, published by Penguin. </p>
</div>

<div class="container mt-5">
  <div class="row">
    <div class="col-sm-6">
      <div class="card mt-4">
        <img class="card-img-top" src="@asset('images/motion-design.gif')" alt="">
        <div class="card-body">
          <h4 class="card-title">Motion Design</h4>
          <p class="card-text">Whatever your idea, we can tell the story in motion.</p>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card mt-4">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://drive.google.com/file/d/0B4iSbJFJuW1zWUM0c1Zrc3lwODQ/preview"></iframe>
        </div>
        <div class="card-body">
          <h4 class="card-title">Explainer Videos</h4>
          <p class="card-text">Explainer videos are a great way to quickly introduce your product to new visitors.</p>
        </div>
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="card mt-4 stripe-top">
        <div class="card-body">
          <h4 class="card-title">Video Editing</h4>
          <p class="card-text">Fast and affordable video editing service.</p>
          <p class="card-text">
          Upload the files to us and let us handle
            the editing and post production.</p>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card mt-4 stripe-top">
        <div class="card-body">
          <h4 class="card-title">Digital Marketing</h4>
          <p class="card-text">Let us handle distribution and focus on shooting more videos. YouTube SEO, thumbnail design and more services available.</p>
        </div>
      </div>
    </div>
  </div>
  <p class="lead mt-5">Get in touch for a chat and we can figure out how we can help you with your project. </p>
  <a href="contact" class="btn btn-primary btn-block btn-lg cta mt-4 mb-4">Start your project today</a>
</div>

@endsection
