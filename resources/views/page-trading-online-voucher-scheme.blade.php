@extends('layouts.app') @section('content')

<div class="jumbotron jumbotron-fluid hero-banner">
  <div class="container">
    <h1>Trading Online Voucher Scheme</h1>
    <p class="lead">Are you ready to start selling online?</p>
  </div>
</div>

<div class="container">
  <div class="col-sm-10">
    <p class="lead">
      If you are running a small to medium size business in Ireland, you may be eligible for a government grant worth up to €2,500
      to help you get your business trading online: 50% of eligible costs up to the maximum of €2,500.</p>

    <p>You can use the voucher to develop or upgrade an e-commerce website, buy online advertising, implement a digital marketing
      strategy or consult with marketing experts to grow your brand online.</p>

    <p>The voucher can also be used for training and digital skills development.</p>

    <p>Over 2,000 businesses have already taken advantage of this great scheme. Those businesses that participated reported
      that they saw average sales increases of 20% and an 80% increase in sales leads.</p>
    <h3>Requirements</h3>
    <p class="lead">To qualify for the scheme, your business must have:</p>
    <ul>
      <li>no more than 10 employees,</li>
      <li>less than €2 million in turnover,</li>
      <li>be trading for a least 12 months,</li>
      <li>be located in the region of the local enterprise office to whom you are applying.</li>
    </ul>
    <p class="lead">
      The vouchers cannot be used for:</p>
    <ul>
      <li>Development of brochure websites</li>
      <li>Purchases of non-internet related software</li>
      <li>Anything other than online trading related activity.</li>
    </ul>
    &nbsp;
    <h3>How To apply</h3>
    <p>
      To apply for a voucher, you must attend one of the Trading Online Voucher information seminars that take place across the
      country.
    </p>
    <p>
      You can book your place at the Wicklow Local Enterprise Office through
      <a href="https://www.localenterprise.ie/Wicklow/Training-Events/Online-Bookings/">this link.</a>
    </p>

    <div class="alert alert-primary">
    You must attend a Trading Online Voucher Seminar in order to apply.
    </div>

    <div class="alert alert-primary">The next deadline for receipt of applications of business who have attended the Trading Online Seminar: is <span class="font-weight-bold">Thursday 29th March at 3pm.</span>
    </div>

    <p>This scheme will run throughout 2018 and more seminar dates and application deadlines will follow.</p>
    <p>If you need some help, 
      <a href="https://focalise.ie/contact/">get in touch with us</a> here at Focalise and we can guide you through the process.</p>

    <div class="downloads py-5">
      <h4>Materials</h4>
      <a class="btn btn-primary btn-lg" href="https://focalise.ie/wp-content/uploads/2018/03/trading-online-voucher-scheme-wicklow.pdf">Download the information PDF</a>
    </div>

    <a class="btn btn-primary" href="https://www.localenterprise.ie/Wicklow/Training-Events/Online-Bookings/TOV-Seminar-08-03-2018.html">Book your place for The Wicklow Trading Online Voucher Seminar</a>

  </div>
</div>

<!-- @include('partials.testimonials') -->

@endsection
