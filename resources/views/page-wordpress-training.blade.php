@extends('layouts.app') @section('content')

<div class="jumbotron gradient-bg jumbotron-fluid">
  <div class="container">
    <div class="col-md-10 offset-md-1">
      <i class="fab fa-wordpress fa-6x mb-4"></i>
      <h1>WordPress Training</h1>
      <p class="wp-training-desc">Learn how to build and manage websites with WordPress.</p>
      <p>Classes are available in Dublin and Wicklow at a location that suits you.</p>
      <a class="btn btn-lg btn-primary" href="{{ home_url('/contact/') }}">Book a lesson</a>
    </div>
  </div>
</div>
<div class="container">
  <div class="col-md-8 offset-md-2">
    <p class="lead">Our WordPress training lessons are suitable for beginners or more advanced. If you are new to WordPress, our introductory classes will bring you up to sped quickly and get you comfortable with running a WordPress website. If you are already familiar with WordPress and are looking to develop your knowledge further, our customised lessons will help you get the most out of your website.</p>
    <p>No experience required. As long as you can use a computer, you can create a great website with WordPress.</p>
    <a class="btn btn-primary" href="https://focalise.ie/contact/">Book a lesson</a>
    <section class="takeaways">
    <h3>Takeaways</h3>
    <p class="lead">Some of the topics that we can cover in our private or group lessons:</p>
      <section>
        <h4>
          <i class="fa fa-caret-right mr-2"></i>WordPress Dashboard Tour</h4>
        <p>Get a tour of all the features of the WordPress Dashboard. The Dashboard is the first page you'll see when you log in to your WordPress site, and it's hidden from your website's visitors. The dashboard holds all the controls you'll need to edit and manage your website. Getting comfortable with the dashboard is the first step to WordPress mastery.</p>
      </section>

      <section>
        <h4>
          <i class="fa fa-caret-right mr-2"></i>How to buy a domain name</h4>
        <p class="lead">Your very own
          <em>www.mybrand.com</em>
        </p>
        <p>Running a website on your own domain name gives your brand a home on the internet, one that you control, rather than
          a page inside another site, like Facebook.</p>
          <p>Learn where to buy domain names, figure out whether you want a .com or a .ie, learn how to get your website to load up on your new domain name, and learn about domain renewals and privacy. 
          </p>
      </section>

      <section>

        <h4>
          <i class="fa fa-caret-right mr-2"></i>How to set up web hosting</h4>
        <p class="lead">Where your website lives</p>
        <p>Your website needs somewhere to live, so we'll look at how to buy web hosting and set it up so your website is available
          worldwide, 24/7.</p>
      </section>

      <section>
        <h4>
          <i class="fa fa-caret-right mr-2"></i>WordPress Installation &amp; Setup</h4>
        <p class="lead">Setting up the engine for your website</p>
        <p>WordPress is the software that will power your website. We'll set up WordPress from scratch, and then we'll explore
          how to create new pages, blog posts, add images, videos, audio and more.</p>
      </section>
      <section>

        <h4>
          <i class="fa fa-caret-right mr-2"></i>Creating and organising your posts and pages.</h4>
        <p class="lead">Learn how to create professional looking pages and posts.<p>
        <p>Learn how to style your content and create professional looking posts and pages. We'll also look at how to organise your content and how to build an easy to use website.</p>
      </section>

      <section>

        <h4>
          <i class="fa fa-caret-right mr-2"></i>Adding Plugins</h4>
        <p class="lead">Learn how to add new features to your website using WordPress plugins.</p>
	<p>Plugins let you extend and add new features to your website. Plugins can help you with contact forms, search engine optimisation, handling spam comments and plenty more. Many of these plugins are free and are easy to add to your site.</p>
      </section>
      <section>
        <h4>
          <i class="fa fa-caret-right mr-2"></i>Learn Search Engine Optimisation</h4>
        <p class="lead">How to build a high quality website that ranks well in the search results.</p>
        <p>There are lots of factors that go into ranking well in the search results, but we'll look at the important basics
          - great content, website performance and backlinks.</p>
      </section>

      <section>
        <h4>
          <i class="fa fa-caret-right mr-2"></i>How to Add Images, Audio &amp; Video</h4>
        <p>Adding media to your website is easy. You can put videos from YouTube, Vimeo or Vine straight into your posts, add images from Instagram or add an audio recording
          from your computer. </p>
      </section>
    </section>

    <section class="bootcamps my-5">
      <div class="card card-shadow stripe-top">
        <div class="card-body">
          <div class="card-title">
            <h2>1-Day WordPress Bootcamp</h2>
          </div>
          <div class="card-text">
            <p class="lead">Fast paced course, aimed at covering as much ground as possible. Great for entrepreneurs and staff. </p>
            <p>In this course you’ll develop a custom site starting from scratch.</p>
            <p>By the end of the day, you'll have a basic website running live on the internet.</p>
            <p>
              <span class="font-weight-bold">Freebies:</span> The price includes 1 year of web hosting and .com domain registration (worth €130) and a full
              lunch and coffee/tea from The Happy Pear.</p>
          </div>
        </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <i class="fa fa-map-marker mr-2"></i>Location: The Happy Pear, Greystones
              </li>
              <li class="list-group-item">
                <i class="fa fa-clock mr-2"></i>Length: 4 hours
              </li>
              <li class="list-group-item">
                <i class="fa fa-credit-card mr-2"></i>Price: €275 per person
              </li>
            </ul>
      </div>
    </section>

  </div>
</div>

<div class="container">
  <div class="col-md-8 offset-md-2">
    <h2 class="display-4">Getting more advanced</h2>
    <p class="lead">If you already have a lot of experience with WordPress, we can cover some more advanced topics.</p>
    <section>
      <h4>Learning Metrics</h4>
      <p>It&#8217;s pretty helpful to learn about what content on your site is the most popular with your visitors, and who
        your visitors are. Metrics can be helpful guides to building a great website. We&#8217;ll teach you how to add Google
        Analytics to your site in order to unlock a wealth of information, like:</p>
      <ul>
        <li>Who your visitors are.</li>
        <li>Where they come from.</li>
        <li>How long they stay on the site.</li>
        <li>Which of your posts are the most popular.</li>
        <li>What search terms people are using to find your site.</li>
      </ul>
      <p>For users that are already used to using Google Analytics, we can get more advanced with services like Hot Jar, Kissmetrics, Google
        Tag Manager and more.</p>
    </section>
    <section>
      <h4>Installing and Customising Themes</h4>
      <p>With WordPress, you can change your entire site layout and
        color scheme by installing a theme. There are free themes and paid themes available. Many of these themes have
        been put together by excellent designers, look professional and don't take much time to set up.</p>
      
      <p>If you're searching for a theme, have a look at our guide:
        <a href="https://focalise.ie/where-to-find-wordpress-themes">Where to find WordPress themes</a>.</p>
      <p>At your lesson, we can explore how to find a suitable theme for your project, and how to get it installed and configured
        correctly for your website.</p>
    </section>
    <section>
      <h4>Moderating Comments</h4>
      <p>Do you ever find yourself reading comments?</p>
      <p>If you like, you can let people leave comments on your posts, so they can ask questions and tell you what they&#8217;re
        thinking. It's a simple way to get in touch with your audience and comment sections often add value to the page.
        You can choose to approve or disapprove each comment as they come in, so you&#8217;re always in control of what&#8217;s
        written on your website. </p>
    </section>
    <section>
      <h4>
        <i class="glyphicon glyphicon-user"></i> Users</h4>
      <p>Maybe you&#8217;d like to get a larger team working on your website. Once again, WordPress makes it easy. You can create
        as many user accounts as you need for contributing authors or editors. We&#8217;ll show you how to create these accounts,
        and how to make sure your users can&#8217;t do anything to the website they&#8217;re not supposed to. </p>
    </section>
    <section>
      <h4>WordPress Plugins</h4>
      <p>So there are 40,000+ plugins. Which ones do you need?</p>
      <p>We&#8217;ll look at some of the most popular plugins, from performance enhancing cache systems to social media integrations.
        There are also loads of
        <abbr title="Search Engine Optimisation">SEO</abbr> plugins.</p>
      <p>While plugins are an easy way to add functionality to your site, they can cause a bit of trouble if you&#8217;re not
        careful. They can slow down WordPress and introduce some security problems. Have a look at our list of
        <a href="{{ home_url('/recommended-wordpress-plugins/') }}">recommended WordPress plugins</a>.
      </p>
    </section>
    <section>
      <h4>Dealing with Spam</h4>
      <p>There are some bad guys out there who have written computer programs that search the internet for WordPress sites,
        and once the program finds a website, it starts posting all sorts of nonsense comments, including links to nasty
        websites. We&#8217;ll teach you how to recognise this spam and show you how to deal with it. Once you&#8217;ve got
        an anti-spam system set up you won&#8217;t have to deal with it ever again.</p>
    </section>

    <section>
      <h4>WordPress
        <abbr title="Search Engine Optimisation">SEO</abbr> Tips</h4>
      <p>Google uses hundreds of measurements in order to figure out where rank each website for each query, so climbing the search results ladder can take a lot of
        fiddling. That said, the increased traffic that comes from the number 1 spot generally makes it worth all the hard work. Book a WordPress SEO lesson and we'll make it easy to get started with SEO.   
      </p>
    </section>

    <section>
      <h4>
        <i class="fa mr-2 fa-shopping-cart"></i>WordPress with a shopping cart</h4>
      <p>If you&#8217;d like to sell a product on your site, and have your website take payments, WordPress can save the day
        yet again. For those who are interested, we tour WooCommerce, an excellent free package that converts WordPress sites
        into fully functional web stores, complete with product listings, reviews, PayPal integration and more. </p>
    </section>
    <section>
      <h4>
        <i class="fa mr-2 fa-save"></i>Backups</h4>
      <p>If you&#8217;ve put a lot of time into a website, it makes sense to set up a backup system in case something happens
        to the site. We&#8217;ll show you some great
        <em>set and forget</em> backup tools that will keep your work safe.</p>
    </section>
    <section>
      <h4>
        <i class="fa mr-2 fa-lock"></i>WordPress Security</h4>
      <p>Even better than restoring from a backup, you can ensure nothing happens to your site in the first place by keeping
        your site up to date at all times. </p>
      <p>Whenever a vulnerability in WordPress is discovered by the WordPress development team, they will fix the problem, harden
        WordPress and send out an update notification to all the WordPress sites around the world. If you hit that &#8220;Update
        Now&#8221; button nice and often, your site will hopefully always be one step ahead of any attackers. We&#8217;ll
        talk about making updates automatic, plugin updates and theme updates, and well as installing security plugins.
      </p>
    </section>

    <section>
      <h4 class="mb-3">Free WordPress Resources</h4>
      <ul class="list-group text-uppercase wordpress-resources-links">
        <li class="list-group-item">
          <a href="https://focalise.ie/recommended-wordpress-plugins/">
            <i class="fas fa-caret-right mr-2"></i>Recommended WordPress Plugins</a>
        </li>
        <li class="list-group-item">
          <a href="https://focalise.ie/how-to-make-your-wordpress-site-private/">
            <i class="fas fa-caret-right mr-2"></i>How to make your WordPress site private</a>
        </li>
        <li class="list-group-item">
          <a href="https://focalise.ie/how-to-add-users-to-your-wordpress-site-quick-tutorial/">
            <i class="fas fa-caret-right mr-2"></i>How to add users to your WordPress site</a>
        </li>
      </ul>
    </section>

    @include('partials.wordpress-newsletter-cta')
  </div>
</div>

@endsection
