<section class="what-is-wordpress">
      <h3>What is WordPress?</h3>
      <p><a href="https://wordpress.org">WordPress</a> is an open source content management system. It makes it easy for you to manage a website without having
        to deal with code. WordPress is free to use, modify and commericialise.</p>
      <p>Originally WordPress was designed as blogging platform, but thanks to a huge community of users and developers, it
        has grown into a fullly featured web publishing platform. It can run nearly any kind of website, from a shop to a
        magazine, porfolio or blog.</p>
      <p>WordPress is easy to set up, manage and update. It is easy to add functionality to the site as your business develops,
        without having to hire a web developer.</p>
      <p>WordPress has become an immensely important tool for businesses across the globe, as it makes it much easier for non-technology
        companies to publish on the internet.</p>
      <p>Once you learn WordPress, you&#8217;ll be able to create website after website, easily. With WordPress, you can build
        sites for side projects, events, consulting services etc.</p>
      <p>Many businesses have room for improvement in their digital strategy, and WordPress is a marketing weapon of choice
        for some of the biggest brands in the world.
        <a href="https://www.sonymusic.com">Sony Music</a>,
        <a href="https://beyonce.com">Beyonce</a> and
        <a href="https://blogs.reuters.com/us/">Reuters</a> all run WordPress. </p>
    </section>