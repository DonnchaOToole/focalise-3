@extends('layouts.app') @section('content')


<div class="jumbotron jumbotron-fluid contact-hero">
    <div class="container">
        <h1>Buy a gift voucher</h1>
        <p class="contact-phone"><i class="fa fa-phone mr-2"></i><span class="bold">+353 (0) 83 448 6980</span>
        </p>
    </div>
</div>

<div class="container mb-5">
        <form action="//formspree.io/donncha@focalise.ie" method="POST" autocomplete="on">
            <div class="form-group">
                <label for="name">Your name</label>
                <input autofocus class="form-control" type="text" name="name" required>
            </div>
            <div class="form-group">
                <label for="_replyto">Your email address</label>
                <input class="form-control" type="email" name="_replyto" required>
            </div>
            <div class="form-group">
                <label>Your phone number</label>
                <input class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="message">How can we help?</label>
                <textarea class="form-control" name="message" rows="5" required></textarea>
            </div>
            <div>
                <input class="btn btn-primary btn-lg" type="submit" value="Send
                ">
            </div>
            <input class="form-control" type="hidden" name="_next" value="//focalise.ie/thanks" />
            <input class="form-control" type="text" name="_gotcha" style="display:none" />
        </form>
</div>

@include('partials.testimonials')
    
@endsection

