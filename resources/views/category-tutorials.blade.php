@extends('layouts.app') @section('content')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1>Tutorials</h1>
  </div>
</div>

  <?php 
if ( have_posts() ) : ?>
  <?php

 if ( category_description() ) : ?>
    <div class="archive-meta">
      <?php echo category_description(); ?>
    </div>

<?php endif; ?>

<div class="container">
  <?php

while ( have_posts() ) : the_post(); ?>
    <div class="col-sm-8">
      <div class="card mb-5">
        <?php echo get_the_post_thumbnail( $page->ID, 'full', array('class' => 'card-img-top img-fluid') ); ?>
        <div class="card-body">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>" class="card-title">
            <h4 class="card-title">
              <?php the_title(); ?>
            </h4>
          </a>
          <p class="card-text">
            <?php the_excerpt(); ?>
          </p>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary card-link">View Tutorial</a>
        </div>
      </div>
    </div>
    <!--                     <div class="entry">
                        <?php the_content(); ?>
                    </div> -->
    <?php endwhile; 

else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
</div>
</section>

</div>

@endsection
