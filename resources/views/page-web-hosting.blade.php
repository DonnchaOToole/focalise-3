@extends('layouts.app') @section('content')

<div class="jumbotron gradient-bg web-hosting-hero jumbotron-fluid">
  <div class="container">
    <h1>Focalise Hosting</h1>
    <p class="lead">Performant web hosting for reliable websites.</p>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-10">
    <section class="managed-servers">
        <h3><i class="fa fa-check mr-2"></i>Private Servers</h3>
        <p class="lead">Concentrate on growing your business while we ensure your website loads quickly and securely. </p>
        <p>Unlike most other hosting companies, we are  </p>
      </section>
      <section class="managed-servers">
        <h3><i class="fa fa-check mr-2"></i>Managed Servers</h3>
        <p class="lead">Concentrate on growing your business while we ensure your website loads quickly and securely. </p>
        <p>We ensure your website server is up to date and performing correctly so you can concentrate on your business rather
          than dealing with server configuration.</p>
      </section>
      <section class="monitored">
        <h3><i class="fa fa-check mr-2"></i>Uptime Monitoring</h3>
          and ready to respond to your visitors. </p>
      </section>
      <section class="cdn">
        <h3><i class="fa fa-check mr-2"></i>Content Distribution Network</h3>
        <p>When visitors are located far away from the server that a website runs on, the website may be slow to load.</p>
        <p>Focalise Hosting uses a content distribution network to speed up your website. </p>
        <p>It works by automatically
          generating copies of your website and hosting them on servers distributed around the globe, so your visitors always see a quick loading website.</p>
      </section>
      <section>
      <h3>Pricing</h3>
      <p>Packages start at €9.99 per month.</p>
      </section>
      <a class="btn btn-primary" href="<?php echo get_home_url();?>/contact/">Get in touch</a>
      
      <!-- 
      <section class="grow"> -->
        <!-- <h3><i class="fa fa-check mr-2"></i>Ready to grow.</h3>
        <p>A big spike in visitor traffic can overwhelm your website and knock it offline if your server isn't powerful enough
          to process all the requests. We offer a web hosting package that can be scaled up quickly to deal with increased
          demand. </p> -->
      </section>
    </div>
    
</div>

<!-- <div class="jumbotron faq-jumbotron jumbotron-fluid mt-5 mb-5">
  <div class="container">
    <h2>Frequently Asked Questions</h2>
  </div>
</div>
<div class="container">
  <div class="row">

    <div class="col-md-6">
      <h3>What is managed web hosting?</h3>
      <p>Managed web hosting is when the web host takes care of the operation of the server so you don&#8217;t have to.</p>
      <p>If you
        don&#8217;t want to deal with the hassle of server configuration, managed web hosting may be for you. </p>
      </div>
      <div class="col-md-6">
        <h3>What is a server?</h3>
        <p>Website hosting is all about where your website lives.</p><p>We can&#8217;t put the website on your laptop because that&#8217;s
          off sometimes and doesn&#8217;t always have a great internet connection. </p><p>So we need another computer to "serve" your
          website to visitors. We call this computer the server. It stays awake and on the internet all the time, ready to send
          your website to anyone who asks for it.</p>
        </div>
      </div>
        
  <section class="improve-your-rankings">
    <div class="alert alert-info">
      <h4>Improve your search rankings.</h4>
      Since Google are always looking to send their customers to sites with a good user experience, sites that take longer to load
      are pushed down the rankings.</p>
      <p>Speeding up your site can ensure you give your users a great experience and improve your search rankings.</p>
    </div>
  </section>
</div> -->


  @endsection
