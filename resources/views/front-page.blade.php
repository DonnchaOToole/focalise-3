@extends('layouts.app') 
@section('content')

<div class="gradient-bg py-5">
    <div class="container py-5">
        <h1 class="hero-text">Websites that drive business</h1>
        <p class="hero-subtext">
            <span class="bold">Mobile-ready websites designed to grow your business.</p>
        <a href="{{ home_url('/contact/') }}" class="btn btn-secondary btn-lg">Get a free consultation</a>
    </div>
</div>
<div class="container">
<!--@include('partials.trading-online-voucher-cta') -->

<section class="training py-5">
        <h3 class="section-title">
            <i class="fa fa-rocket training-icon mr-3" aria-hidden="true"></i>Learn WordPress</h3>
        <p class="lead">Learn how to make beautiful, custom websites using Wordpress. We offer group courses and customised training for individuals and groups of staff. In-person classes are available in Dublin and Wicklow, and online classes are available if you are located elsewhere.</p>
        <a href="{{ home_url('/wordpress-training/') }}" class="btn btn-secondary btn-lg mt-2">
            <i class="fa fa-info-circle mr-3" aria-hidden="true"></i>Learn More</a>
</section>

<section class="photography py-5">
        <h3 class="section-title">
            <i class="fa fa-camera photography-icon mr-3" aria-hidden="true"></i>Photography</h3>
        <p class="lead">Complete photography services.</p>
        <p>Portraits, Family Photography, Event Photography, Promotional Photography, Pet Photography.</p>
        <a href="{{ home_url('/photography/') }}" class="btn btn-secondary btn-lg mt-2">
            <i class="fa fa-info-circle mr-3" aria-hidden="true"></i>Learn More</a>
</section>


</div>    


@include('partials.testimonials')

    
@endsection

