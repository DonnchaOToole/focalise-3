@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="container">
    <h1 class="display 1">404</h1>
    <h2>Page Not Found</h2>
    <div class="alert alert-warning">
      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
    </div>
    <h3>What happened?</h3>
<p>A 404 error status implies that the file or page that you're looking for could not be found.</p>

<h3>What can I do?</h3>
<p>Please use your browser's back button, or click the button below to head back to the home page.</p>
<p>If you need immediate assistance, please send us a message through <a href="{{ home_url('/contact/') }}">our contact form</a> or send an email to support@focalise.ie.</p>
    <a class="btn btn-lg btn-primary" href="{{ home_url('/') }}">Back to the Home Page</a>
    </div>
    <!-- {!! get_search_form(false) !!} -->
  @endif
@endsection
