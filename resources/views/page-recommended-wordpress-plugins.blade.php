@extends('layouts.app') @section('content')

<div class="jumbotron gradient-bg jumbotron-fluid">
  <div class="container">
    <div class="col-md-10 offset-md-1">
      <i class="fab fa-wordpress fa-6x mb-4"></i>
      <h1>Recommended WordPress Plugins</h1>
      <a class="btn btn-primary" href="{{ home_url('/wordpress-training/') }}">WordPress Training</a>
    </div>
  </div>
</div>

<div class="container">
  <div class="col-md-10 offset-md-1">

    <p class="wp-training-desc">Our list of essential WordPress plugins to help you get the most out of your website.</p>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>Cerber Security &amp; Antispam</h2>
      <p class="free-paid">Freemium</p>
      <p>Professional grade WordPress security and anti-spam plugin. Protects website against brute force attacks. Block harmful
        requests. Restricts access with a Black IP Access List and a White IP Access List. Tracks user and intruder activity
        with powerful email and mobile notifications.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://wpcerber.com/">https://wpcerber.com/</a>
      </p>
    </div>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>Yoast SEO</h2>
      <p class="free-paid">Freemium</p>
      <p>Add functionality to your website that will help you to improve your position in the organic search results.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://yoast.com/wordpress/plugins/seo/">https://yoast.com/wordpress/plugins/seo/</a>
      </p>
    </div>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>WP Super Cache</h2>
      <p class="free-paid">Free</p>
      <p>This plugin generates static html files from your dynamic WordPress blog. After a html file is generated your webserver
        will serve that file instead of processing the comparatively heavier and more expensive WordPress PHP scripts.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://wordpress.org/plugins/wp-super-cache/">https://wordpress.org/plugins/wp-super-cache/</a>
      </p>
    </div>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>WP Smush</h2>
      <p class="free-paid">Freemium</p>
      <p>Large image files may be slowing down your site without you even knowing it. WP Smush uses WPMU DEV’s super servers
        to quickly smush every single one of your images and cuts all the unnecessary data without slowing down your site.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://wordpress.org/plugins/wp-smushit/">https://wordpress.org/plugins/wp-smushit/</a>
      </p>
    </div>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>Smush Image Compression and Optimization</h2>
      <p class="free-paid">Freemium</p>
      <p>Large image files may be slowing down your site without you even knowing it. Smush Image Compression and Optimization
        (formerly known as WP Smush) uses quickly smushes every single one of your images and cuts all the unnecessary data
        without slowing down your site.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://wordpress.org/plugins/wp-smushit/">https://wordpress.org/plugins/wp-smushit/</a>
      </p>
    </div>

    <div class="plugin">
      <!-- <img src="https://placehold.it/1200x500" alt="" class="img-fluid"> -->
      <h2>Broken Link Checker</h2>
      <p class="free-paid">Free</p>
      <p>This plugin will monitor your site looking for broken links and let you know if any are found.</p>
      <p class="official-website">
        <i class="fa fa-link"></i>Official Website:
        <a href="https://wordpress.org/plugins/broken-link-checker/">https://wordpress.org/plugins/broken-link-checker/</a>
      </p>
    </div>
  </div>
</div>


@endsection
