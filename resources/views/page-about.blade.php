@extends('layouts.app') @section('content')


<div class="jumbotron jumbotron-fluid gradient-bg">
  <div class="container">
    <h1>About Focalise</h1>
    <p class="lead">We design and build digital experiences and services.</p>
  </div>
</div>
<div class="container">
  <div class="col-md-8">
    <p class="lead">We're here to help you
      <span class="font-weight-bold">build your brand</span> and
      <span class="font-weight-bold">reach more customers</span>.</p>
      <p>Started by Donncha O'Toole in 2015, Focalise is based in Greystones Co. Wicklow.</p>
      <p>We've helped local and national businesses with developing their digital services and growing
      sales.</p>
    <p>If you'd like to grow your business with
      <a href="{{ home_url('/web-design/') }}">web design</a>, <a href="{{ home_url('/photography/') }}">photography</a> or <a href="{{ home_url('/web-hosting/') }}">web hosting</a>, and <a href="{{ home_url('/wordpress-training/') }}">training</a>, please get in touch.</p>
    
    <!-- <p><i class="fa fa-phone mr-2"></i>+353 (0) 83 448 6980</p> -->

    <a href="https://focalise.ie/contact/" class="btn btn-lg btn-primary">Get in touch</a>
    <section class="opening-hours my-5">
      <h3 class="section-header">Opening Hours</h3>
      <table class="table table-striped table-hover table-sm">
        <thead>
          <tr>
            <th>Day</th>
            <th>Hours</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Monday</th>
            <td>9:00am - 5:30pm</td>
          </tr>
          <tr>
            <th scope="row">Tuesday</th>
            <td>9:00am - 5:30pm</td>
          </tr>
          <tr>
            <th scope="row">Wednesday</th>
            <td>9:00am - 5:30pm</td>
          </tr>
          <tr>
            <th scope="row">Thursday</th>
            <td>9:00am - 5:30pm</td>
          </tr>
          <tr>
            <th scope="row">Friday</th>
            <td>9:00am - 5:30pm</td>
          </tr>
          <tr>
            <th scope="row">Saturday</th>
            <td>Closed</td>
          </tr>
          <tr>
            <th scope="row">Sunday</th>
            <td>Closed</td>
          </tr>
        </tbody>
      </table>
    </section>
  </div>
</div>

@include('partials.testimonials') @endsection
