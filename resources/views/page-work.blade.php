@extends('layouts.app') @section('content')


<div class="jumbotron jumbotron-fluid gradient-bg">
  <div class="container">
      <i class="fa fa-laptop fa-5x mb-4"></i>
      <h1>Featured Web Design Projects</h1>
      <p class="lead">Every project is different and we strive to build <span class="bold">effective</span> digital experiences that <span class="bold">boost business</span> for each of our clients. </p>
  </div>
</div>


<div class="container">

<section class="farmgas case-study">
<div class="row">
  <div class="col-6">
    <h4>FarmGas</h4>
    <p class="lead">We designed and developed a responsive website to help FarmGas find new business partners. </p>
    <ul class="list-group mb-4">
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Mobile Ready</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>HTTPS</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Bespoke Design</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Contact Form</li>
    </ul>
    <a target="_blank" href="https://farmgas.ie" class="btn btn-primary">Visit website in a new tab</a>
  </div>
  <div class="col-6">
    <img src="@asset('images/farmgas.png')" alt="Image of FarmGas Website" class="img-fluid case-study-thumb">
  </div>
</div>
  
</section>
<section class="lucynuzumweddings case-study">
<div class="row">
  <div class="col-6">
  <h4 class="card-title">Lucy Nuzum Weddings</h4>
  <p class="lead">Lucy needed a mobile ready website to display her photography style to newly engaged couples. </p>
  <ul class="list-group mb-4">
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Mobile Ready</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Bespoke Design</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Contact Form</li>
    </ul>
  <a target="_blank" href="http://lucynuzumweddings.com" class="btn btn-primary">Visit website in a new tab</a>
  </div>
  <div class="col-6">
    <img src="@asset('images/lucynuzumweddings.png')" alt="Image of Lucy Nuzum Weddings Website" class="img-fluid case-study-thumb">
  </div>
</div>
</section>

<section class="indianspice case-study">
<div class="row">
  <div class="col-6">
  <h4 class="card-title">Indian Spice Company</h4>
  <p class="lead">Indian Spice Company is an Indian Takeaway based in Greystones and Newtownmountkennedy.</p>
  <ul class="list-group mb-4">
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Mobile Ready</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>HTTPS</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Bespoke Design</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Contact Form</li>
    </ul>
  <a target="_blank" href="https://www.indianspiceco.com" class="btn btn-primary">Visit website in a new tab</a>
  </div>
  <!-- <div class="col-6">
    <img src="@asset('images/indianspiceco.png')" alt="Image of Indian Spice Company Website" class="img-fluid case-study-thumb">
  </div> -->
</div>
</section>

<section class="avsolutions case-study">
<div class="row">
  <div class="col-6">
  <h4 class="card-title">AV Solutions</h4>
  <p class="lead">AV Solutions is an audio visual company specialising in smart home installations.</p>
  <ul class="list-group mb-4">
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Mobile Ready</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>HTTPS</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Bespoke Design</li>
        <li class="list-group-item"><i class="fa fa-check mr-2"></i>Contact Form</li>
    </ul>
  <a target="_blank" href="https://avsolutions.ie" class="btn btn-primary">Visit website in a new tab</a>
  </div>
  <!-- <div class="col-6">
    <img src="@asset('images/avsolutions.png')" alt="Image of AV Solutions" class="img-fluid case-study-thumb">
  </div> -->
</div>
</section>

@include('partials.trading-online-voucher-cta')


@include('partials.testimonials')


@include('partials.ready-to-get-started')

</div>


@endsection
