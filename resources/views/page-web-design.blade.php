@extends('layouts.app') @section('content')

<!-- <img class="img-fluid" alt="focalise web design" src="@asset('images/focalise-web-design.jpg')"> -->
<section class="web-design-intro gradient-bg py-5">
  <div class="container">
    <div class="col-md-10 offset-md-1">
      <h1>Effective Web Design</h1>
      <p class="lead">We design and build fast, functional websites designed to turn visitors into customers.</p>
      <a href="{{ home_url('/contact/') }}" class="btn btn-primary btn-lg">Book a free consultation</a>
    </div>
  </div>
</section>
<div class="container">
  <div class="col-md-10 offset-md-1">
    <section class="responsive">
      <h3 class="section-title">Responsive Web Design</h3>
      <p>Your laptop screen is a different size and shape from your phone’s screen, but your website needs to look great on
        both devices.
      </p>
      <p>Our websites are built with mobile in mind, and resize themselves to fit any screen size, from tiny mobiles to massive
        desktop monitors.</p>
      <p>
        <span class="text-bold">This is important these days since more than 50%</span> of web traffic is now coming in from mobile devices.
      </p>
    </section>

    <section class="ecommerce">
      <h3 class="section-title">E-Commerce</h3>
      <p class="lead">Create a web store that is open to accept orders and payments 24/7.</p>
      <p>Whether you've got 1 product or 10,000+ products to sell, we have a solution for you.</p>
    </section>
    <section class="secure-payments">
      <h3 class="section-title">Secure Payment Systems</h3>
      <p class="lead">Accept credit card payments
        <span class="bold">securely</span> without putting your customers at risk.</p>
      <p>We integrate secure payment gateways from
        <a href="https://stripe.com">Stripe</a>,
        <a href="https://paypal.com"> Paypal</a>,
        <a href="https://www.braintreepayments.com/">Braintree</a> and
        <a href="https://www.realexpayments.com/ie/">Realex</a> so you don't have to worry about storing and protecting your customers credit card details.</p>
      <ul class="list-group">
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Recurring Payments</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Invoices</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Fraud Protection</li>
      </ul>
    </section>

    <section class="ux">
      <h3 class="section-title">User Experience Design</h3>
      <p class="lead">Focusing on user experience (UX) helps to maximise conversions and boost customer engagement.</p>
      <p>As a website owner, the greatest risk is that your users will get frustrated or bored and close the browser tab, never
        to return again.</p>
      <p>Using market analysis, user testing, persona creation and user flows, we work to make your website as user friendly
        as possible. Wave goodbye to your high bounce rate.</p>
    </section>

    <section class="cms">
      <h3 class="section-title">Easy Content Management</h3>
      <p>You need a great way to organise the content on your website so you don’t need to contact a web developer every time
        you have a new promotion.
        <p>
          <a href="https://www.wordpress.org">WordPress</a> is the first choice for many of our clients. It&#8217;s easy to use, extensible and powers many of
          the most popular websites on the internet.
        </p>
        <p>We offer custom theme design services,
          <a href="https://focalise.ie/web-hosting/">web hosting</a> and also
          <a href="{{ home_url('/wordpress-training/') }}">WordPress training</a>.

    </section>
    <section class="landing-pages">
      <h3 class="section-title">Effective Landing Pages</h3>
      <p class="lead">Great landing page designs tend to minimise distractions and make process of signing as enjoyable and simple as possible.</p>
      <p>
        <a href="{{ home_url('/landing-page-design/') }}">Landing pages</a> are web pages designed with a single objective in mind, carefully crafted to guide the user to
        take the next step, whether that be signing up to your service or entering a contest.</p>
      <p>If your product needs a landing page,
        <a href="{{ home_url('/contact/') }}">get in touch</a> for a chat today.</p>
    </section>
    <section class="our-process">
      <h3 class="section-title">The Process</h3>
      <p class="lead">We start with a strong vision of the finished product and that drives the whole project.</p>
      <p>We've established a tried and tested web design process to ensure that every website we build delivers great value
        for our clients:</p>
      <ul class="list-group">
        <li class="list-group-item">1. We start off with research into your target market and clarification of the key goals of the project.</li>
        <li class="list-group-item">2. Once we have a solid plan, we develop our first draft of the website, and this acts as a basis for discussion
          and feedback.
        </li>
        <li class="list-group-item">3. When the design is signed off, the draft design is developed into the final, fully featured website, ready for
          action.</li>
        </ol>
    </section>

    @include('partials.trading-online-voucher-cta') @include('partials.testimonials') @include('partials.ready-to-get-started')

  </div>
</div>
<!-- <section class="hosting">
    <div class="jumbotron">
      <div class="container">
        <h3 class="section-title">
          <i class="fa fa-bolt"></i> Managed Hosting Packages</h3>
        <p class="lead">Ensure your website responds quickly to your customers with our managed hosting packages.</p>
        <a href="<?php echo get_home_url();?>/web-hosting/" class="btn btn-outline-primary btn-lg">Web Hosting Info</a>
      </div>
    </div>
  </section> -->
<!-- <div class="row"> -->
<!-- <div class="col-sm-6">
      <h3 class="section-title">Web Development Services</h3>
      <p class="lead">Take your business to the next level.</p>
      <p>We provide a wide range of web development services to support your business. </p>
      <a class="mb-5  btn btn-lg btn-outline-primary" href="https://focalise.ie/contact/">
        <i class="fa fa-cog fa-spin"></i> Start your project</a>
    </div> -->
<!-- <div class="col-sm-6">
      <ul class="services-list list-group mt-4 mb-4">
        <li class="list-group-item">
          <a href="https://focalise.ie/how-to-improve-a-website/">Website Improvements</a>
        </li>
        <li class="list-group-item">
          Marketing Automation
        </li>
        <li class="list-group-item">
          Social Media Integrations
        </li>
        <li class="list-group-item">
          HTTPS/SSL Certificate Installations
        </li>
      </ul>

    </div> -->
<!-- </div> -->

@endsection
