@extends('layouts.app') @section('content')

<!-- <img class="img-fluid mt-3 pt-2 visible-sm-up" src="<?php bloginfo('template_directory');?>/dist/images/PODCAST-EDITING-SERVICE.jpg"> -->
<img src="@asset('images/podcast-editing-service.jpg')" alt="Podcast Editing Service" class="img-fluid">
<div class="container">
  <div class="row">
    <div class="col-lg-8 offset-lg-2 pt-5">
      <h1>Podcast Rocket
        <i class="ml-3 rocket-icon fa fa-rocket"></i>
      </h1>

      <div id="typed"></div>
      <p>We can edit your podcast, save you time and help you make your podcast the best it can be. </p>
      <ul class="list-group podcast-intro-list my-5">
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Improve Intelligibility.</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Eliminate Unwanted Sounds.</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Insert Intros, Outros &amp; Sponsored Messages.</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>ID3 Tagging &amp; Cover Art Design</li>
        <li class="list-group-item">
          <i class="fa fa-check mr-2"></i>Transcription</li>
      </ul>
      <section class="how-it-works">
        <h3 class="my-3 text-center how-it-works mb-5">How it works</h3>
        <div class="row">
          <div class="col-sm-4">
            <i class="fa fa-circle fa-3x mb-3"></i>
            <h4>Record</h4>
            <p>You record your podcast and collect all the audio files into a folder.</p>
          </div>
          <div class="col-sm-4">
            <i class="fa fa-arrow-up fa-3x mb-3"></i>
            <h4>Upload</h4>
            <p>Upload that folder to us and tell us what you're looking for.</p>
          </div>
          <div class="col-sm-4">
            <i class="fa fa-coffee fa-3x mb-3"></i>
            <h4>Enjoy</h4>
            <p>The finalised episode MP3 is delivered direct to your Google Drive or Dropbox.</p>
          </div>
        </div>
      </section>
      <section class="editing">
        <h2>
          <i class="fa fa-volume-up pr-4 "></i>Audio Editing &amp; Enhancement</h2>
        <p class="lead">Getting that radio sound.</p>
        <p>We will edit, EQ, apply compression, and generally do whatever we have to do in order to get your episode sounding
          great. The levels should be balanced nicely, the dialogue should be clear and easy to listen to. </p>
        <p>We can remove many unwanted sounds or accidents that may have crept into your recordings, but unfortunately not all
          recordings are salvagable. If a bad recording is causing problems, we'll get in touch and figure out a solution.
        </p>
      </section>

      <section class="mixed">
        <h2>
          <i class="fa fa-headphones mr-2"></i>Mixed &amp; Tested</h2>
        <p>Episodes are mixed and checked on professional studio monitors and also iPhone earbuds to ensure all of your listeners
          are getting a great sonic experience, no matter where they are listening.</p>

      </section>

      <section class="turnaround">
        <h2>
          <i class="fa fa-clock mr-2"></i>Quick Turnarounds</h2>
        <p>We will have your episode ready within 72 hours.
          <h5 class="mt-4">
            <i class="fa fa-fast-forward mr-2"></i> Need it finished sooner?</h5>
          <p>We also have a fast track package with a 24 hour turnaround. Perfect for shows that need to go to air as fast as
            possible. Only €19 extra per episode.</p>

      </section>
      <section class="pricing">
        <div class="f-callout">
          <h2>Pricing</h2>
          <p class="lead"></p>
          <p>Only €79 per episode (up to 90 mins). First episode is completely free.</p>
          <a href="<?php echo get_home_url();?>/contact/" class="btn btn-primary">Start your project</a>
        </div>

      </section>
      <section class="show-notes">
        <h3 class="mt-4">Show Notes
          <small>Add-On</small>
        </h3>
        <p class="lead">Impress your listeners with great professional show notes for just €19.</p>
        <p>Our show notes writers will help you summarize your episode, neatly listing the various links, books and ideas that
          were mentioned in the episode. </p>
      </section>
      <section class="cover-art">
        <h3 class="mt-4">Podcast Cover Art
          <small>Add-On</small>
        </h3>
        <p class="lead">Stand out in iTunes with professionally designed cover art starting at just €49</p>
        <p>Your podcast artwork is the first thing your listeners will see, and great designs can bring in lots of listeners.</p>
        <ul class="list-group mb-4">
          <li class="list-group-item">
            <i class="fa fa-check mr-2"></i>Scaled correctly for iTunes</li>
          <li class="list-group-item">
            <i class="fa fa-check mr-2"></i>Simple, clear designs.</li>
          <li class="list-group-item">
            <i class="fa fa-check mr-2"></i>Smartphone ready</li>
        </ul>
      </section>

      <!--<section class="audio-training">
        <p class="lead">Audio Training</p>
        <p>We also offer training on recording technique. Please <a href="<?php echo get_home_url();?>/contact/">send us a message</a>          if you'd like to have a chat about your recording set up.</p>
        </section>-->
      <section class="start-your-project">
        <h2>Ready To Go?</h2>
        <p class="lead">Send us a message and we'll organise a consultation over Skype where we can go over the details of your show. </p>
        <a class="btn btn-primary mb-4 btn-block btn-lg" href="<?php echo get_home_url();?>/contact/">Start your project</a>
      </section>

      <section class="recommended-podcasts">
        <p>Looking for a new podcast to get into? Have a look at our
          <a href="{{ home_url('/recommended-podcasts/') }}">recommended podcasts list</a>.</p>
      </section>
    </div>
  </div>
</div>

<script>
  var app = document.getElementById('typed');

  var typewriter = new Typewriter(app, {
    loop: true
  });

  typewriter.typeString('The straight-forward podcast editing service.')
    .pauseFor(2500)
    .deleteChars(16)
    .typeString('production service.')
    .pauseFor(2500)
    .start();

</script>

@endsection
