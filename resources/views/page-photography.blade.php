@extends('layouts.app') @section('content')
  <img src="@asset('images/a.jpg')" alt="" class="img-fluid img-shadow">
<div class="jumbotron jumbotron-fluid gradient-bg">
  <div class="container">
    <div class="col-md-8 offset-md-2">
      <h1>Focalise Photography</h1>
      <p class="lead">Complete photography services.</p>

    </div>
  </div>
</div>

<div class="container">
  <div class="col-md-8 offset-md-2">

  <h2 class="mt-4">Photography Services</h2>
    <ul class="list-group photography-services">
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Portraits &amp; Headshots</li>
     <li class="list-group-item"><i class="fa fa-check photography-check"></i><a href="https://focalise.ie/actor-headshots">Actor Headshots</a></li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Wedding Photography</li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Newborn &amp; Baby Photography</li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Family Photography</li>        
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Event Photography</li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Product Photography</li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Promotional Photography</li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i><a href="https://focalise.ie/pet-photography/">Pet Photography</a></li>
        <li class="list-group-item"><i class="fa fa-check photography-check"></i>Photo Grading Services</li>
    </ul>
    <p>For information on any of our photography products, you can get in touch with our <a href="https://focalise.ie/contact/">contact form</a> or call 083 448 6980. We have gift vouchers available.</p>

  <img src="@asset('images/david-and-stephen-flynn-the-happy-pear.jpg')" alt="Photo of David and Stephen Flynn, Owners of The Happy Pear, Greystones." class="img-fluid img-shadow my-4 mt-4">

<h3>Professional Headshots for business</h3>

<p>When it comes to business you are going to need to make a good first impression, whether that is from your website or your LinkedIn profile.</p>
<p>We offer quick and easy portrait styles to suit you and what your business is about.</p>

<h4>What we need</h4>
<ul>
<li>Tell us a bit about you and your business, we want to achieve an image that communicates who you are. This can help us decide if an indoor or outdoor shoot is more desirable.</li>
<li>We will need 10-15 minutes of set up time for our lights and backdrop.</li>
<li>Depending on how many images you require the photo shoot can last anywhere from 20 minutes up to an hour.
20-30 minutes to select the images you like.</li>
</ul>
<h4>What you receive:</h4>
<ul>
<li>You will be given direction and professional poses to use during the shoot so you look your best.</li>
<li>High resolution digital Images to share and post on your website and profiles.</li>
<li>Print release</li>
</ul>

@include('partials.get-a-free-quote')

  <img src="@asset('images/b.jpg')" alt="" class="img-fluid img-shadow my-4">
  <img src="@asset('images/c.jpg')" alt="" class="img-fluid img-shadow my-4">

  <img src="@asset('images/d.jpg')" alt="Newborn baby photo" class="img-fluid img-shadow my-4">
  <img src="@asset('images/f.jpg')" alt="" class="img-fluid img-shadow my-4">
  <img src="@asset('images/g.jpg')" alt="Rapeseed fields in Greystones, Co. Wicklow, 2018" class="img-fluid img-shadow my-4">

  

  <img src="@asset('images/h.jpg')" alt="Bray Airshow 2017" class="img-fluid img-shadow my-4">

  </div>
</div>
@endsection
