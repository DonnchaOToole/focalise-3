@extends('layouts.app') 
@section('content')

<div class="container privacy-policy">
    <h1>Privacy Policy</h1>
    <h2 id="cookies">Cookies</h2>
<p>This website uses cookies in order to enhance your user experience. By using this website you are agreeing to the use of cookies from this website.</p>
<p>Cookies are small text files that websites use to remember information about you. They’re stored on your computer and can only be accessed by the website that created them or your web browser. It is possible to change the cookie settings of your browser to allow or block cookies depending on your preference.</p>
<p>This website uses a number of different cookies depending on how you use the website:</p>
<p>Google Analytics
(a third-party cookie)</p>
<p>_utma
_utmb
_utmc
_utmz</p>
<p>These cookies are set by Google Analytics and are used to collect information about how visitors use our site. We use this information to compile reports and to help us improve the site. The cookies collect information in an anonymous form, including the number of visitors to the site, where visitors have come to the site from and the pages they visited.</p>
<p>Session Cookie
(a first-party cookie)</p>
<p>These cookies are set by this website and are essential to provide visitors with the information they request as they navigate through the site.</p>
<p>The cookies that are used for Google Analytics in order for us to understand what content people find useful, and for us to then provide better content on the website. This information is collected in anonymous form and includes information such as visits to the site, how you arrived at the site and what pages you visited. If you wish to opt out of this then follow this link <a href="https://tools.google.com/dlpage/gaoptout">https://tools.google.com/dlpage/gaoptout</a>.</p>
<p>Blocking the use of cookies may result in this website not functioning properly.</p>
<p>3rd Party Analytics or re targeting that we are using
Google Tag Manager
Google Analytics</p>
<h2 id="privacy-policy">Privacy Policy</h2>
<p>This privacy policy has been compiled to better serve those who are concerned with how their ‘Personally Identifiable Information’ (PII) is being used online. PII, as described in US and EU privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
<h2 id="what-personal-information-do-we-collect-from-the-people-that-visit-our-blog-website-or-app-">What personal information do we collect from the people that visit our blog, website or app?</h2>
<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number or other details to help you with your experience.</p>
<h2 id="when-do-we-collect-information-">When do we collect information?</h2>
<p>We collect information from you when you fill out a form or enter information on our site.</p>
<h2 id="how-do-we-use-your-information-">How do we use your information?</h2>
<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
<ul>
<li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
</ul>
<p>How do we protect your information?</p>
<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.</p>
<p>We use regular Malware Scanning.</p>
<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
<p>We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.</p>
<p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
<h2 id="do-we-use-cookies-">Do we use ‘cookies’?</h2>
<p>Yes. Cookies are small files that a site or its service provider transfers to your computer’s hard drive through your Web browser (if you allow) that enables the site’s or service provider’s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
<p>We use cookies to:</p>
<ul>
<li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</li>
</ul>
<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser’s Help Menu to learn the correct way to modify your cookies.</p>
<p>If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.It won’t affect the user’s experience that make your site experience more efficient and may not function properly.</p>
<h2 id="third-party-disclosure">Third-party disclosure</h2>
<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</p>
<h2 id="third-party-links">Third-party links</h2>
<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
<h2 id="google">Google</h2>
<p>Google’s advertising requirements can be summed up by Google’s Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a></p>
<p>We use Google AdSense Advertising on our website.</p>
<p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google’s use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>
<p>We have implemented the following:</p>
<ul>
<li>Remarketing with Google AdSense</li>
<li>Google Display Network Impression Reporting</li>
<li>Demographics and Interests Reporting</li>
<li>DoubleClick Platform Integration</li>
</ul>
<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>
<h2 id="opting-out-">Opting out:</h2>
<p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>
<p>How does our site handle Do Not Track signals?
We don’t honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. We don’t honor them because:
We do not have the facility to do that.</p>
<h2 id="does-our-site-allow-third-party-behavioral-tracking-">Does our site allow third-party behavioral tracking?</h2>
<p>It’s also important to note that we do not allow third-party behavioral tracking</p>
<h2 id="coppa-children-online-privacy-protection-act-">COPPA (Children Online Privacy Protection Act)</h2>
<p>When it comes to the collection of personal information from children under the age of 13 years old, the Children’s Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States’ consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children’s privacy and safety online.</p>
<p>We do not specifically market to children under the age of 13 years old.</p>
<h2 id="fair-information-practices">Fair Information Practices</h2>
<p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
<p>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:
We will notify you via email</p>
<ul>
<li>Within 7 business days</li>
</ul>
<p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>
<h2 id="can-spam-act">CAN SPAM Act</h2>
<p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
<p>We collect your email address in order to:</p>
<ul>
<li>Send information, respond to inquiries, and/or other requests or questions</li>
<li>Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</li>
</ul>
<p>To be in accordance with CANSPAM, we agree to the following:</p>
<ul>
<li>Not use false or misleading subjects or email addresses.</li>
<li>Identify the message as an advertisement in some reasonable way.</li>
<li>Include the physical address of our business or site headquarters.</li>
<li>Monitor third-party email marketing services for compliance, if one is used.</li>
<li>Honor opt-out/unsubscribe requests quickly.</li>
<li>Allow users to unsubscribe by using the link at the bottom of each email.</li>
</ul>
<p>If at any time you would like to unsubscribe from receiving future emails, you can email us at</p>
<ul>
<li>Follow the instructions at the bottom of each email.
and we will promptly remove you from ALL correspondence.</li>
</ul>
<h2 id="contacting-us">Contacting Us</h2>
<p>If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
<p>focalise.ie
<br>A63 W562
<br>Greystones, Wicklow
<br>Ireland
<br>support@focalise.ie
<br>083 448 6980</p>


</div>


@endsection
