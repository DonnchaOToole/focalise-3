<section class="testimonials">
  <div class="text-center testimonials-header mb-4">
    <h3 class="section-title">Hear from our happy customers</h3>
  </div>

  <div class="container">
    <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="row">
            <div class="col-sm-3">
              <img class="img-fluid testimonials-img rounded mb-3" alt="Photo of David and Stephen Flynn, owners of The Happy Pear in Greystones, Co. Wicklow."
                src="@asset('images/the-happy-pear.jpg')">
            </div>
            <div class="col-sm-9">
              <blockquote class="blockquote d-block w-100">
                <i class="fa fa-quote"></i>We have been working with Donncha for the last 10 years and he is brilliant, very fast and insightful
                and wonderful to deal with! We highly recommend him!
                <div class="mt-3 blockquote-footer">
                  <cite>Stephen Flynn,
                    <a href="https://thehappypear.ie">The Happy Pear</a>
                  </cite>
                </div>
              </blockquote>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="row">
            <div class="col-sm-3">
              <img src="@asset('images/stephen.jpg')" alt="Photo of Stephen Branagan" class="img-fluid testimonials-img rounded mb-3">
            </div>
            <div class="col-sm-9">
              <blockquote class="blockquote d-block w-100">
                <i class="fa fa-quote"></i>We have worked with Dunny on everything from website design to graphics to IT infrastructure and have
                found him to be a very valuable addition to our team. Very knowledgeable, very responsive and nothing is
                too much trouble. Highly recommended!
                <div class="mt-3 blockquote-footer">
                  <cite>Stephen Branagan,
                    FarmGas
                  </cite>
                </div>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
