<footer class="page-footer mt-5 stripe-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h5 class="location">Greystones, Ireland</h5>
        <p class="phone-number">083 448 6980</p>
        <p class="email">sales@focalise.ie</p>
      </div>

      <div class="col-md-4">
        <div class="footer-column">
          <h5>Services</h5>
          <ul class="list-group footer-list">
            <li class="list-group-item">
              <a href="{{ home_url('/web-design/') }}">Web Design</a>
            </li>
            <li class="list-group-item">
              <a href="{{ home_url('/training/') }}">Training</a>
            </li>
            <!-- <li class="list-group-item">
              <a href="{{ home_url('/video/') }}">Video Production</a>
            </li> -->
            <li class="list-group-item">
              <a href="{{ home_url('/photography/') }}">Photography</a>
            </li>
            <li class="list-group-item">
              <a href="{{ home_url('/seo-services/') }}">SEO</a>
            </li>
          </ul>
        </div>

      </div>
      <div class="col-md-4">
        <div class="footer-column">
          <h5>Company</h5>
          <ul class="list-group footer-list">
            <li class="list-group-item">
              <a href="{{ home_url('/about/') }}">About</a>
            </li>
            <li class="list-group-item">
              <a href="{{ home_url('/contact/') }}">Contact</a>
            </li>
            <li class="list-group-item">
              <a href="{{ home_url('/tutorials/') }}">Tutorials</a>
            </li>
            <li class="list-group-item">
              <a href="{{ home_url('/resources/') }}">Resources</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  @include('partials.social-media-cta')
  <div class="container">
    <div class="copyright text-center pb-2">
      &copy; 2015-<?php echo date("Y"); ?> Focalise | 
      <a href="{{ home_url('/privacy-policy/') }}">Privacy Policy</a>
    </div>

  </div>

</footer>
