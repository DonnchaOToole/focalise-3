<div class="text-center social-media-links">
  <div class="btn-group" role="group" aria-label="follow us buttons">
    <a href="https://twitter.com/focalise_ie" class="btn btn-secondary">
      <i class="fab fa-twitter mr-2"></i>Twitter</a>
    <a href="https://www.youtube.com/channel/UCAo1-eYYQL7UzYgigO8hHVQ" class="btn btn-secondary">
      <i class="fab fa-youtube mr-2"></i>YouTube</a>
  </div>
</div>
