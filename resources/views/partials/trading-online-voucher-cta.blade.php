
  <section class="trading-online-voucher py-5">
    <h3 class="section-title">
      <i class="fa fa-rocket training-icon mr-3" aria-hidden="true"></i>Trading Online Voucher Scheme</h3>
    <p class="lead">Your business may be eligible for a government grant worth up to
      <span class="font-weight-bold">€2,500</span> to help you build your business online. </p>
    <a href="{{ home_url('/trading-online-voucher-scheme/') }}" class="btn btn-secondary btn-lg mt-2">
      <i class="fa fa-info-circle mr-3" aria-hidden="true"></i>More info</a>
  </section>
