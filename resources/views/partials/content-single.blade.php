<article @php(post_class())>
  <header>
    <div class="jumbotron jumbotron-fluid gradient-bg">
      <div class="container">
      <div class="col-md-8 offset-md-2">

        <h1 class="entry-title">{{ get_the_title() }}</h1>
        @include('partials/entry-meta')
      </div>
    </div>  
    </div>
  </header>
  <div class="container">
    <div class="col-md-8 offset-md-2">
      <div class="entry-content post-content">
        @php(the_content())
      </div>
      <footer class="mt-4">
        {!! wp_link_pages(['echo' => 0, 'before' => '
        <nav class="page-nav">
          <p>' . __('Pages:', 'sage'), 'after' => '</p>
        </nav>']) !!}
      </footer>
      @php(comments_template('/partials/comments.blade.php'))
    </div>
  </div>
</article>
