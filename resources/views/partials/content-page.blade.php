<div class="container">
<div class="col-sm-8 offset-sm-2">
<h1>{{ get_the_title() }}</h1>
@php(the_content())
</div>
</div>
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
 