<div class="jumbotron wordpress-training-signup">
  <div id="mc_embed_signup">
    <form action="https://focalisesoftware.us7.list-manage.com/subscribe/post?u=5ad740079e1860da55c5d2271&amp;id=126b9df9ca"
      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
      <div id="mc_embed_signup_scroll form-group">
        <h4>Stay up to date</h4>
        <p>Follow this class to get an email the next time it's scheduled.</p>
        <div class="signup-inputs">
          
          <div class="mc-field-group">
            <label for="mce-EMAIL">Email Address
              <span class="asterisk">*</span>
            </label>
            <input placeholder="example@gmail.com" type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
          </div>
          <div class="mc-field-group">
            <label for="mce-FNAME">First Name</label>
            <input placeholder="Jane" type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
          </div>
          <div class="mc-field-group">
          <label for="mce-LNAME">Last Name</label>
          <input placeholder="Fitzgerald" type="text" value="" name="LNAME" class="form-control" id="mce-LNAME">
        </div>
      </div>
        <div id="mce-responses" class="clear">
          <div class="response" id="mce-error-response" style="display:none"></div>
          <div class="response" id="mce-success-response" style="display:none"></div>
        </div>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true">
          <input type="text" name="b_5ad740079e1860da55c5d2271_126b9df9ca" tabindex="-1" value="">
        </div>
        <div class="clear">
          <input type="submit" value="Start Following" name="subscribe" id="mc-embedded-subscribe" class="mt-3 button btn btn-primary">
        </div>
      </div>
    </form>
  </div>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
  (function ($) {
    window.fnames = new Array();
    window.ftypes = new Array();
    fnames[0] = 'EMAIL';
    ftypes[0] = 'email';
    fnames[1] = 'FNAME';
    ftypes[1] = 'text';
    fnames[2] = 'LNAME';
    ftypes[2] = 'text';
    fnames[3] = 'ADDRESS';
    ftypes[3] = 'address';
    fnames[4] = 'PHONE';
    ftypes[4] = 'phone';
  }(jQuery));
  var $mcj = jQuery.noConflict(true);

</script>
<!--End mc_embed_signup-->
