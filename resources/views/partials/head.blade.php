<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56357572-2"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-56357572-2');
  </script>
<script type="text/javascript">
(function(w,d){
  w.HelpCrunch=function(){w.HelpCrunch.q.push(arguments)};w.HelpCrunch.q=[];
  function r(){var s=document.createElement('script');s.async=1;s.type='text/javascript';s.src='https://widget.helpcrunch.com/';(d.body||d.head).appendChild(s);}
  if(w.attachEvent){w.attachEvent('onload',r)}else{w.addEventListener('load',r,false)}
})(window, document)
</script>

<script type="text/javascript">
  HelpCrunch('init', 'focalise', {
    applicationId: 4671,
    applicationSecret: 'uaDIZYQuLugR1rEYFZ0j/Z3YBJA9xt6LvSJH2m1V8SgPIg95sQrnA9BbDXMZZd5IzzWTrrv3sohtPpeUHByjTg=='
  });

  HelpCrunch('showChatWidget');
</script>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="google-site-verification" content="vvD644gcT4PR5xZxX_Xi7vfNykS5HV4NVCdhNwvh3tI" />
  <link async href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <script async src="https://cdnjs.cloudflare.com/ajax/libs/TypewriterJS/1.0.0/typewriter.min.js"></script>

  @php(wp_head())
</head>
