
<nav class="navbar navbar-expand-lg navbar-light bg-faded">
  <a class="navbar-brand" href="{{ home_url('/') }}">
  <img src="@asset('images/focalise-logo-1.svg')" width="150" height="30" alt="">
  </a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="{{ home_url('/') }}">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="{{ home_url('/about/') }}">About</a>
      <a class="nav-item nav-link" href="{{ home_url('/web-design/') }}">Web Design</a>
      <!-- <a class="nav-item nav-link" href="{{ home_url('/video/') }}">Video</a> -->
      <a class="nav-item nav-link" href="{{ home_url('/training/') }}">Training</a>
      <a class="nav-item nav-link" href="{{ home_url('/photography/') }}">Photography</a>
      <a class="nav-item nav-link" href="{{ home_url('/resources/') }}">Resources</a>
      <a class="nav-item nav-link" href="{{ home_url('/contact/') }}">Get in touch</a>
    </div>
  </div>
</nav>
