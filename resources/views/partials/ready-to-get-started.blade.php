<section class="call-out ready-to-get-started my-5">
<div class="jumbotron card-shadow stripe-top">
    <h4 class="mb-3 display-5">Ready to get started?</h4>
    <a class="mb-3 btn btn-lg btn-block btn-primary" href="{{ home_url('/contact/') }}">Start Your Project</a>
</div>
</section>