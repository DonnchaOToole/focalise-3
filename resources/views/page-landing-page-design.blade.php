@extends('layouts.app') @section('content')

<div class="jumbotron jumbotron-fluid hero-banner landing-page-hero">
<div class="container">
<h1 class="landing-page-hero-text">Landing Page Design</h1>
<p class="lead">Stop Wasting Time On Landing Pages That Don't Convert</p>
</div>
</div>
<div class="container">
<div class="col-md-8 offset-md-2">
<section>
<h3>What is a landing page?</h3>
<p class="lead">A landing page is a web page with a single, <span class="bold">focused</span> objective. 
<p>Whereas the home page of your website might have lots of links to other pages on your site such as your blog or your about page, a landing page design will often aim to minimise these distractions and leave the user with one clear next action.</p>
<p>When a user completes the task that the landing page was designed for, we call that a <span class="bold">conversion</span>.</p>
<p>Landing pages are sometimes called "lead capture pages", "destination pages" or, simply, "landers". You might find yourself on a landing page if you were to click on a Google Ad, and you should find that the messaging on the page should be a natural extension of the headline of the ad you clicked on.</p>
<p>Landing pages are sometimes shared to social media, or used in an email marketing campaign. In general, the goal of a landing page is to convert website visitors into sales or leads.</p>
<p>A great landing page is engaging and straightforward.</p>
</section>
<section>
<h3>5 uses for landing pages:</h3>
<ul>
    <li>Pair a landing page perfectly with an AdWords campaign to boost your <a href="https://support.google.com/adwords/answer/1752122?hl=en">AdRank</a> and get more clicks.</li>
    <li>Enable your visitors to book consultations.</li>
    <li>Enable your vistors to enter a contest or giveaway.</li>
    <li>Give away a free trial of your product in return for contact details.</li>
    <li>Collect email addresses for a future product launch.</li>
</ul>
</section>
</div>
<section class="landing-page-inspiration">
<div class="row">
<div class="col-md-7">
<h3>Landing Page Inspiration</h3>
<p class="lead"><a href="https://stripe.com/checkout">Here is an example</a> of a great landing page from <a href="https://stripe.com">Stripe</a>, an internet payments company founded by two Irish brothers, Patrick and John Collison. This landing page is for one of Stipe's products called Checkout.</p>
<p>As you can see, it is focused and clear. The messaging on the page is all related to Checkout, it doesn't distract the user with Stripe's other products, and is dedicated to convincing the user to start the sign up process.</p>
</div>
<div class="col-md-5">
<img src="<?php bloginfo('template_directory');?>/dist/images/stripe-checkout.png" alt="" class="img-fluid">
</div>
</div>
</section>        
<div class="col-sm-10 offset-md-1">
<section>
<h3>Great landing page designs minimise distrations</h3>
<p>If you are launching a new app, your landing page might be centered around a download button, or if you are growing your newsletter, the page might be designed to get convince to enter their email.</p>
<p>By minimising distractions for the user, a good landing page design can boost conversions, and is ideal for matching up with AdWords campaigns.</p>
<p>The messaging on the page should be clear and easy to understand. If you get it right, your visitors will decide within a few seconds to go ahead and click on your call to action button.</p>
<p>If they find it difficult to understand what you are offering, they will likely back out of your website all together.</p>
<p>You will need:</p>
<ul>
<li>
A great headline.</li>
<li>Enticing, simple messaging.</li>
<li>A clear, simplified design.</li>
</ul>
</section>
<section>
<h3>Why choose Focalise?</h3>
<p>If you want a clear, consise landing page put together and you don't want to worry about the design and the hosting and the domain names, simply let us know what you need from the landing page and we'll do everything else.</p>
<p>We offer complete <a href="https://focalise.ie/web-design/">web design services</a> to help your business grow.</p>
</section>
<section>
<h3>No hosting hassles</h3>
<p>You don't need to worry about configuring your <a href="https://focalise.ie/web-hosting>">website hosting</a>, we can handle that for you. </p>
</section>
<a class="btn btn-primary btn-lg" href="https://focalise.ie/contact">Book your free consultation</a>
<!--<p>Here's an example. Bill has an online store selling audio equipment, and he has started a campaign on AdWords to try to boost his headphones sales. He creates an ad that says:</p>
<code>July Special: 25% off Sennheiser headphones.</code>
<p> At the moment, all the ads he has created point to the home page of his website. The problem with this is that when a user clicks the ad, they   </p>-->

<!--<p>
 To further incentivise the user to click the "call to action" i.e. "Sign Up", many landing pages will offer something in return, such as a free guidebook or free sample.</p>-->
<!--<p>These pages which are designed to capture the users email address are called Lead Generation pages. The focus of the page is to capture that email address, so all unnecessary distractions are removed, and a clear inviting headline intices the user to enter their email address. </p>-->

</div>
</div>


@endsection
