@extends('layouts.app') @section('content')
<div class="jumbotron jumbotron-fluid gradient-bg">
  <div class="container">
    <div class="col-md-8 offset-md-2">
      <i class="fa fa-laptop fa-5x mb-4"></i>
      <h1>Computer Repair</h1>
      <p class="lead">If something has gone wrong with your PC or Mac, we can help you get everything back to normal again.</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="col-md-8 offset-md-2">
    <p class="lead">
      We repair problems with both hardware and software on PC or Apple computers.
    </p>
    @include('partials.get-a-free-quote')

    <section class="no-fix-no-fee">
      <h4>
        <i class="fa fa-check mr-2"></i>No Fix No Fee</h4>
      <p class="lead">
        We have a
        <strong>no fix, no fee </strong>guarantee, so we can't fix your problem, there is absolutely no charge.
      </p>
    </section>


    <section class="call-out">
      <h4>
        <i class="fa fa-check mr-2"></i>Low call out fees</h4>
      <p class="lead">
        <strong>Low call out fees </strong>- If you'd prefer to have your computer repaired on site, we offer a low cost call out
        service.
      </p>
    </section>

    <section class="call-out">
      <h4>
        <i class="fa fa-truck mr-2"></i>Pick Up</h4>
      <p class="lead">
        We can collect your machine and drop it back to you after the repair for a low fee.
      </p>
      <p>The collection fee depends on your location, please <a href="https://focalise.ie/contact/">get in touch</a> for a quote.</p>
    </section>

<section class="call-out">
      <h4>
        <i class="fab fa-apple mr-2"></i>Mac Repair</h4>
      <p class="lead">
      Fast and affordable Mac Repair.
      </p>
    </section>

      @include('partials.get-a-free-quote')

    <section class="screen-repair">
      <h4>Screen Repair</h4>
      <p>It's quite easy to drop your laptop and shatter your screen, it happens to the best of us.</p>
      <p>We can fit a replacement screen and get your laptop back working again. The price depends on the type of screen,
      <a href="{{ home_url('/contact/') }}">get in touch</a> for a free quote.
    </section>


    <section class="ssd">
      <h4>SSD Upgrades</h4>
      <p>One of the most effective ways to speed up an aging laptop is to fit it with a solid state drive.</p>
      <p>Solid state drives (SSDs) have no moving parts and can read and write much faster than the older spinning disk hard
        drives.</p>
      <p>SSDs also draw less power than traditional hard drives and can give a 30+ minute battery boost.</p>
      <p>As a classic style hard drive gets older, its moving parts degrade and it takes longer and longer to do its job.</p>

      <p>This results in the computer freezing up when you open big programs or files. Fitting an SSD can result in a dramatic
        improvement in computer responsiveness.</p>

      <p>Our customers often report that their laptops feel as good as new after we've installed an SSD.</p>

      <p>We move all of the data from your old hard drive onto your new SSD, including your operating system, so all your files
        and settings will be unchanged. You won't notice anything different except for a major boost in speed.</p>

    </section>

    <section class="keyboards">
      <h4>Keyboard Replacement</h4>
      <p>
        A broken keyboard can render a laptop almost useless. Thankfully most models have replacement keyboards available, so we
        can revive that laptop you thought was defunkt. Prices vary depending on the model of laptop.
      </p>

      <p>
        <a href="{{ home_url('/contact/') }}">Send us</a> the laptop's model number and we'll get back to you with a free quote, or call +353 83 448 6980.</p>
    </section>

    <section class="fans">
      <h4>Fan Replacement</h4>
      <p>
        As laptops get older, they tend to have some trouble keeping their cool. As time goes by, the fan can ingest quite a bit
        of dust even in the most spotless homes. If your laptop sounds like it's preparing to take off down a runway,
        <a href="{{ home_url('/contact/') }}">get in touch with us</a> for a service.
      </p>
    </section>

    <section class="virus-repair">
      <h4>Virus Repair</h4>
      <p>Many viruses and malware can be removed from your computer without the loss of your files.</p>

      <p>We're sad to say this isn't always the case, however. Some malware can result in damage to your data which cannot be
        repaired, and often
        <a href="{{ home_url('/what-is-ransomware/') }}">ransomware</a> is the culprit. If that's the case, our
        <strong>no fix no fee </strong>guarantee takes effect.
      </p>
    </section>

    <section class="services">
      <h2>MORE SERVICES</h2>
      <ul class="list-group">
        <li class="list-group-item">Repair Overheating Issues</li>
        <li class="list-group-item">Broadband & WiFi Problems</li>
        <li class="list-group-item">Data Recovery</li>
        <li class="list-group-item">Data Backup</li>
        <li class="list-group-item">RAM Upgrades</li>
        <li class="list-group-item">Recovering and removing passwords</li>
        <li class="list-group-item">Removal of all data in preparation for resale.</li>
        <li class="list-group-item">Graphics Card Installations and Upgrades</li>
        <li class="list-group-item">Battery Replacement (PC Laptops and Macs)</li>
        <li class="list-group-item">Operating System Updates and Reinstallations</li>
        <li class="list-group-item">Full Computer Service and Tune Up.</li>
        <li class="list-group-item">Home Network Configuration</li>
        <li class="list-group-item">Training for smartphones and tablets (iOS/Android)</li>
        <li class="list-group-item">
          <a href="{{ home_url('/wordpress-training/') }}">WordPress Training</a>
        </li>
        <li class="list-group-item">Email Repair and Recovery</li>
        <li class="list-group-item">
          <a href="{{ home_url('/google-suite/') }}">Google Suite Setup</a>
        </li>
      </ul>
    </section>

      @include('partials.get-a-free-quote')

  </div>
</div>
@endsection
